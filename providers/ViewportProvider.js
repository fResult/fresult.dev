import React, { createContext, useEffect, useState } from 'react'

export const ViewPortCtx = createContext(null)

const ViewportProvider = ({ children }) => {
  const [width, setWidth] = useState(window.innerWidth)
  const [height, setHeight] = useState(window.innerHeight)

  useEffect(() => {
    const handleResize = () => {
      setWidth(window?.innerWidth)
      setHeight(window?.innerHeight)
    }

    window?.addEventListener('resize', handleResize)
    return () => {
      window?.removeEventListener('resize', handleResize)
    }
  }, [])

  return (
    <ViewPortCtx.Provider value={{ width, height }}>
      {children}
    </ViewPortCtx.Provider>
  )
}

export default ViewportProvider