import React, { FC } from 'react'
import { AppProps } from 'next/app'
import { Title, Text, Paragraph } from 'components/atomic'
import { Avatar } from 'antd'

const Home: FC<AppProps> = props => {
  return (
    <div style={{ textAlign: 'center' }}>
      <Title level={1}>Hello World</Title>
      <div style={{ marginBottom: 30 }}>
        <Avatar
          {...{
            src: 'static/fResult500x500.jpg',
            alt: 'Display image',
            style: { height: 200, width: 200 }
          }}
        />
      </div>
      <Paragraph type="danger" style={{ fontSize: 24, textAlign: 'center' }}>
        My name is{' '}
        <Text type={'warning'} underline>
          Sila Setthakan-anan
        </Text>
        .
      </Paragraph>
    </div>
  )
}

export default Home
