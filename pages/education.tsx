import React, { FC } from 'react'
import { Title } from 'components/atomic'
import styled from 'styled-components'
import { Card as CardAntD } from 'antd'
import { GiGraduateCap } from 'react-icons/gi'

const Card = styled(CardAntD)`
  &.ant-card-bordered {
    border: none;
  }

  &.ant-card {
    width: 75%;
    max-width: 500px;
    margin: auto;
    border-radius: 47%;
    background: transparent;
  }
`

const { Meta: MetaAntd } = Card

const Meta = styled(MetaAntd)`
  .ant-card-meta-detail {
    //background: transparent;

    .ant-card-meta-description {
      color: azure;
    }
  }
`

interface EducationProps {
  institute: string
  style: object
}

const Education: FC<EducationProps> = props => {
  return (
    <div style={{ ...props.style, textAlign: 'center' }}>
      <Title level={1}>
        <span style={{ marginRight: 20, fontSize: 42 }}>
          <GiGraduateCap />
        </span>
        Education
      </Title>
      <Card
        {...{
          cover: (
            <img
              src="https://library.stou.ac.th/odi/ODIDistance/img/slide-2.jpg"
              alt="STOU"
              style={{ borderRadius: '47%' }}
            />
          )
        }}
      >
        <Meta
          {...{
            title: (
              <div style={{ textAlign: 'left' }}>
                <Title level={3}>
                  2017 - 2020
                  <br />
                  Sukhothai Thammathirat Open University
                </Title>
              </div>
            ),
            description: (
              <div style={{ textAlign: 'left', fontSize: 'initial' }}>
                <ul style={{ listStyle: 'none' }}>
                  <li>Bachelor degree of Science</li>{' '}
                  <li>Now I'm studying in last semester in...</li>
                  <li>Faculty of Science and Technology</li>{' '}
                  <li>Computer Science major</li>
                  <li>GPA: 2.89</li>
                </ul>
              </div>
            )
          }}
        />
      </Card>
    </div>
  )
}

export default Education
