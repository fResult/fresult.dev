import React, { FC } from 'react'
import 'styles/styles.css'
import { AppProps } from 'next/app'
import Head from 'next/head'
import styled from 'styled-components'
import ViewportProvider from '../providers/ViewportProvider'

import { Layout as LayoutAntD } from 'antd'
import Menu from 'components/Menu'
import PageBackground from 'components/PageBackground'

const {
  Header: HeaderAntD,
  Content: ContentAntD,
  Footer: FooterAntD
} = LayoutAntD

const Layout = styled(LayoutAntD)`
  background: transparent;
  position: fixed;
  top: 0;
  height: 100vh;
`

const Content = styled(ContentAntD)`
  overflow-y: auto;
  height: 100%;
  width: 100%;
  display: flex;
  position: fixed;
  top: 65px;
  align-items: center;
  justify-content: center;
`

const Header = styled(HeaderAntD)`
  background: transparent;
  width: 100vw;
  position: fixed;
  z-index: 10000;
`

const Footer = styled(FooterAntD)`
  background: transparent;
  color: azure;
  position: fixed;
  bottom: 0;
  text-align: center;
  width: 100%;
  z-index: 10000;
`

const App: FC<AppProps> = ({ Component, pageProps }) => {
  return (
    <>
      <ViewportProvider>
        <Head>
          <title>fResult.dev</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <Layout>
          <Header>
            <Menu {...pageProps} />
          </Header>
          <Content>
            <Component
              {...{
                ...pageProps,
                style: {
                  width: '100%',
                  overflowY: 'auto',
                  textAlign: 'center',
                  ...pageProps.style
                }
              }}
            />
          </Content>
          <Footer>Copyright 2020 by fResult Sila</Footer>
        </Layout>
        <PageBackground />
      </ViewportProvider>
    </>
  )
}

export default App
