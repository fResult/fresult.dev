import { useContext } from 'react'
import { ViewPortCtx } from '../providers/ViewportProvider'

const useViewport = () => {
  const { width, height } = useContext(ViewPortCtx)
  return { width, height }
}

export default useViewport
