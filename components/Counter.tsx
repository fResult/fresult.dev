import React, { useState } from 'react'

function Counter() {
  const [count, setcount] = useState(0)
  return (
    <div>
      <h1>Counter - {count}</h1>
      <button onClick={() => setcount(prevCount => prevCount + 1)}>
        Increment
      </button>
    </div>
  )
}

export default Counter
