import React from 'react'
import { AppProps } from 'next/app'
import { Menu as MenuAntD } from 'antd'
import styled from 'styled-components'
import Router from 'next/router'

const Menu = styled(MenuAntD)`
  background: transparent;
  color: azure;

  & .ant-menu-horizontal > .ant-menu-item:hover,
  & .ant-menu-horizontal > .ant-menu-submenu:hover,
  & .ant-menu-horizontal > .ant-menu-item-active,
  & .ant-menu-horizontal > .ant-menu-submenu-active,
  & .ant-menu-horizontal > .ant-menu-item-open,
  & .ant-menu-horizontal > .ant-menu-submenu-open,
  & .ant-menu-horizontal > .ant-menu-item-selected,
  & .ant-menu-horizontal > .ant-menu-submenu-selected {
    color: rgba(240, 255, 255, 0.3) !important;
    border-bottom: 2px solid rgba(240, 255, 255, 0.3) !important;
  }
  & .ant-menu-horizontal > .ant-menu-item,
  & .ant-menu-horizontal > .ant-menu-submenu {
    color: rgba(240, 255, 255, 0.8) !important;
    border-bottom: 2px solid rgba(240, 255, 255, 0.2) !important;
  }
`

export default (props: AppProps) => {
  return (
    <Menu
      mode="horizontal"
      onClick={e => Router.push(`/${e.key !== 'home' ? e.key : ''}`)}
    >
      <Menu.Item key="home">Home</Menu.Item>
      <Menu.Item key="education">Education</Menu.Item>
      <Menu.Item key="work">Work</Menu.Item>
    </Menu>
  )
}
