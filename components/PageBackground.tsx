import React from 'react'
import styled, { keyframes } from 'styled-components'

const AnimationArea = styled.div`
  z-index: -1;
  top: 0;
  position: fixed;
  background-image: linear-gradient(to left, #000000, #121212);
  background-attachment: fixed;
  background-size: cover;
  width: 100%;
  height: 100vh;
  margin: 0 !important;

  ul li:nth-child(1) {
    left: 86%;
    width: 80px;
    height: 80px;
    animation-delay: 0s;
  }

  ul li:nth-child(2) {
    left: 9%;
    width: 30px;
    height: 30px;
    animation-delay: 1.5s;
    animation-duration: 10s;
  }

  ul li:nth-child(3) {
    left: 70%;
    width: 150px;
    height: 150px;
    animation-delay: 5.5s;
  }

  ul li:nth-child(4) {
    left: 42%;
    width: 150px;
    height: 150px;
    animation-delay: 0s;
    animation-duration: 15s;
  }

  ul li:nth-child(5) {
    left: 65%;
    width: 40px;
    height: 40px;
    animation-delay: 0s;
  }

  ul li:nth-child(6) {
    left: 18%;
    width: 110px;
    height: 110px;
    animation-delay: 3.5s;
    animation-duration: 15s;
  }
`

const BoxArea = styled.ul`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  overflow: hidden;
  margin: 0;
`
const animate = keyframes`
    0% {
      transform: translateY(0) rotate(0deg);
      opacity: 1;
    }
    100% {
      transform: translateY(-1000px) rotate(360deg);
      opacity: 0;
    }
`

const Box = styled.li`
  position: absolute;
  width: 25px;
  height: 25px;
  display: block;
  list-style: none;
  background: rgba(255, 255, 255, 0.2);
  animation: ${animate} 20s linear infinite;
  bottom: -150px;
`

const PageBackground = () => {
  return (
    <>
      <AnimationArea>
        <BoxArea>
          <Box />
          <Box />
          <Box />
          <Box />
          <Box />
          <Box />
        </BoxArea>
      </AnimationArea>
    </>
  )
}

export default PageBackground
