import { Title } from 'components/atomic/atoms/Title'
import { Text } from 'components/atomic/atoms/Text'
import { Paragraph } from 'components/atomic/atoms/Paragraph'

export { Title, Text, Paragraph }
