import React, { FC } from 'react'
import { Typography } from 'antd'
import styled from 'styled-components'
import { TitleProps } from 'antd/es/typography/Title'

const { Title: TitleAntD } = Typography

export const Title: FC<TitleProps> = styled(TitleAntD)`
  & {
    color: azure !important;
  }
`
