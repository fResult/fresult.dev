import React, { FC } from 'react'
import { Typography } from 'antd'
import styled from 'styled-components'
import { TextProps } from 'antd/es/typography/Text'

const { Text: TextAntD } = Typography

export const Text: FC<TextProps> = styled(TextAntD)`
  & {
    color: azure !important;
  }
`
