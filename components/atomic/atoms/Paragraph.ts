import React, { FC } from 'react'
import { Typography } from 'antd'
import styled from 'styled-components'
import { ParagraphProps } from 'antd/es/typography/Paragraph'
const { Paragraph: ParagraphAntD } = Typography

export const Paragraph = styled(ParagraphAntD)`
  & {
    color: azure !important;
  }
`
